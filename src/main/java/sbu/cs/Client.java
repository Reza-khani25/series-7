package sbu.cs;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Client {

    /**
     * Create a socket and connect to server then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     * @throws IOException
     */
    private static final int port = 6060 ;

    public static void main(String[] args) throws IOException {
        String filePath = args[0];      // "sbu.png" or "book.pdf"



        byte[] byteArray = readFile(filePath) ;
        send(byteArray , filePath , (int)new File(filePath).length());


    } // end main

    /**
     * reads a file from local repository
     *
     * @param path file's directory
     * @throws IOException
     *
     **/
    public static byte[] readFile(String path) throws IOException {


        File file = new File(path) ;
        byte[] byteArray = new byte[(int)file.length()] ;
        FileInputStream fis = new FileInputStream(file) ;
        BufferedInputStream bis = new BufferedInputStream(fis) ;

        bis.read(byteArray , 0 , byteArray.length) ;

        send(byteArray , path , byteArray.length);
        fis.close();
        bis.close();

        return byteArray;
    }

    /**
     * sends a file via socket connection
     *
     * @param byteArray mentioned file
     * @param filePath file's name
     * @param len file's length
     * @throws IOException
     *
     */
    public static void send(byte[] byteArray , String filePath , int len) throws IOException {
        Socket socket = null ;
        DataOutputStream dout = null ;

        try{
             socket = new Socket("localhost" , port) ;

            System.out.println("connected to server ...");

             dout = new DataOutputStream(socket.getOutputStream()) ;

            dout.writeInt(len); // send file's length
            dout.flush();
            dout.writeUTF(filePath); // send file's name
            dout.flush();

            System.out.println("trying to send " + filePath);

            dout.write(byteArray , 0 , byteArray.length);
            dout.flush();

            System.out.println("sent");

        }catch (IOException e)
        {
            e.printStackTrace();
        }finally {
            if(socket!= null)
                socket.close();

            if(dout != null)
                dout.close();

        }
    }
}
