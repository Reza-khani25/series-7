package sbu.cs;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    /**
     * Create a server here and wait for connection to establish,
     *  then get file name and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException
     */

    private static final int port = 6060;

    public static void main(String[] args) throws IOException, InterruptedException {
        // below is the name of directory which you must save the file in it

        String directory = args[0];     // default: "server-database"
        System.out.println("waiting for a client to connect : ");

        try (ServerSocket ss = new ServerSocket(port); Socket socket = ss.accept()) {

            System.out.println("Connection established ...");

            getData(socket, directory);

            System.out.println("file downloaded & saved in directory");

        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    /**
     * receives a file from client and stores that in the given directory
     * @param socket
     * @param directory
     * @throws IOException
     */
    public static void getData(Socket socket , String directory) throws IOException {
        DataInputStream din = null ;
        FileOutputStream fos = null ;

        byte[] b  ;

        try{
             din= new DataInputStream(socket.getInputStream()) ;

             int len = din.readInt() ;
             b = new byte[len] ;

            String fileName = din.readUTF() ;
            fileName = directory + "/" + fileName;


            fos=new FileOutputStream(new File(fileName),true);

            din.readFully(b);
            fos.write(b);

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (din!=null)
                din.close();
            if (fos!=null)
                fos.close();
        }
    }
}
